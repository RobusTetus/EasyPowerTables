# EasyPowerTables 🔌️
Simple bash script to ease manipulation with ACPI tables.
**This project is very much a work in progress... and not guaranteed to work**
Pull requests and contributions are very much welcome. 💛️

## What?
This software is intended for helping you prepare ACPI tables extracted from any bios file for manual patching. It aims to provide a way of getting around iasl errors like AE_ALREADY_EXISTS (and hopefully more to come).

## Why?
It always bothers me when some part of my machine does not work as it should on Linux as it works on Windows. Mainly it seems because most of computer/laptop manufacturers like to bend the ACPI standards and fill in the gaps with bundling drivers in their Windows installations. However when you are a Linux user, most companies will not help you making the device you bought with Windows bundled work with Linux. In a nutshell, this project goal is to make it easier for "regular" users who are not firmware engineers modify and inspect their ACPI tables. Second reason would be for my own education and gaining expirience on the matter.

## How?
Complete rewrite and overhaul of my [ACPIpatcher](https://gitlab.com/RobusTetus/EasyPowerTables/-/tree/legacy?ref_type=heads) project. It is pretty much a completely different project now. Initially I wanted to modify table .bin files directly before decompilation which turned out to be an incredible pain and inconsistent. So I realized I can decopile everything as is and then do some simpler text processing, such as removing duplicates (to get rid of AE_ALREADY_EXISTS errors and such).

## Usage
### Package requirements for running the script
* `iasl` (commonly part of `acpica` package)
* `7zip`
* `uefitool` (distribution package or downloaded executable can be used with `CUSTOM_UEFIEXTRACT=<path-to-executable>` environment variable, not needed when using docker image below)
* `xxd`
* Common utilities like `grep`, `find` and `coreutils`

### Running the script in Docker/Podman
To extract tables run:
`docker run -v /path/to/biosfile.exe:/bios.exe:ro -v /path/to/output:/output:rw quay.io/robustetus/ept:latest -e bios.exe`

To extract tables on distribution with SELinux, run:
`docker run -v /path/to/biosfile.exe:/bios.exe:z -v /path/to/output:/output:z quay.io/robustetus/ept:latest -e bios.exe`

To extract and decompile tables run:
`docker run -v /path/to/biosfile.exe:/bios.exe:ro -v /path/to/output:/output:rw quay.io/robustetus/ept:latest -d bios.exe`

To extract and decompile tables on distribution with SELinux, run:
`docker run -v /path/to/biosfile.exe:/bios.exe:z -v /path/to/output:/output:z quay.io/robustetus/ept:latest -d bios.exe`

Make sure to replace `/path/to/biosfile.exe` and `/path/to/output` with absolute paths on your host system.

## Todo-list
- [ ] Implement a sane way to detect and remove duplicates from batch of decompiled tables
- [ ] Accept more bios formats (currently tested with only self extracting archives from Lenovo)
- [ ] Clean up and refactor the script

## Thanks
This project uses [UEFIExtract](https://github.com/LongSoft/UEFITool) and [iasl compiler](https://github.com/acpica/acpica).
Big thank you to all who contributed to these projects. I would be lost without these great tools. 😁️

## License
This project is licensed under [GNU GENERAL PUBLIC LICENSE version 3](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text).

