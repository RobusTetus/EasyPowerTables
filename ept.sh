#!/bin/bash

############################################################
# Information
############################################################

# EasyPowerTables by Robin Tetour
#
# Gitlab repository: https://gitlab.com/RobusTetus/EasyPowerTables
# Quay.io images: https://quay.io/repository/robustetus/ept

############################################################
# Dependencies
############################################################

#	ACPICA -- ACPI Component Architecture
#		- https://github.com/acpica/acpica
#	UEFITool -- UEFI firmware image viewer and editor
#		- https://github.com/LongSoft/UEFITool
# Bash shell, GNU Grep, 7z, xxd and Coreutils

############################################################
# Information variables
############################################################

VERSION=0.2.0
SUBJECT=EasyPowerTables
AUTHOR="Robin Tetour"
USAGE="Usage: ept [options] <file>"

############################################################
# Global variables
############################################################

BATCH_DIR="output"

OUTPUT_DIR="${BATCH_DIR}/out"
TEMP_DIR="${BATCH_DIR}/temp"
LOG_DIR="${BATCH_DIR}/logs"

EXTRACTED_INSTALLER="${TEMP_DIR}/UEFI_Installer_Contents"
EXTRACTED_BIOS="${TEMP_DIR}/UEFI_Contents"

RAW_TABLES="${OUTPUT_DIR}/Blob_ACPI_Tables"
DECOMP_TABLES="${OUTPUT_DIR}/Plain_Text_ACPI_Tables"

############################################################
# Dissasembles raw UEFI/BIOS file and moves resulting files into a desired location
# Usage: disassemble-raw-bios <file> <location>
############################################################
disassemble-raw-bios()
{
  if [[ $# == 2 ]]; then
    uefiextract $1 dump > $LOG_DIR/uefiextract.log

    if [[ -d $1.dump ]]; then
      write-out "Image $(basename $1) successfully disassembled." success
      cp -u -r $1.dump/* $2
    else
      write-out "Could not move extracted files." error 1
    fi

  else
    write-out "disassemble-raw-bios <file> <location>" error 1
  fi
}

############################################################
# Uses iasl compiler to decompile every raw ACPI table in a
# specified directory and moves them to a desired output directory
# Usage: decompile-tables <folder-with-tables> <output-folder>
############################################################
decompile-tables()
{
  if [[ $# -ne 2 ]]; then
    write-out "decompile-tables <folder> <output-folder>" error 1
  fi

  if [[ -d $1 ]]; then
    mkdir -p $2

    for i in $1/*; do
      iasl -d $i &>> "$LOG_DIR/iasl.log"
      if [[ $VERBOSE -eq 1 ]]; then
        write-out "$i decompiled into $2"
      fi
    done

    mv $1/*.dsl $2/
    write-out "Tables decompiled succesfully." success
  fi
}

############################################################
# Find duplicate string
# TODO
############################################################
find-duplicates()
{
  write-out "Duplicate detection is to be implemented\n" warn
}

############################################################
# Parse and remove text
# TODO
############################################################
delete-text()
{
  write-out "Duplicate deletion is to be implemented\n" warn
}

############################################################
# Function that unzips self-extracting archives of uefi installers
# into desired location then finds and sets BIOS_BINARY variable 
# e.g. Lenovo UEFI/BIOS .exe installers
# Usage: unzip-bios-installer <bios-file> <destination>
############################################################
unzip-bios-installer()
{
  if [[ $# == 2 ]]; then
    7z e -aos $1 -o$2 > $LOG_DIR/7zip.log
    write-out "Installer $(basename $1) extracted." success
    BIOS_BINARY=$(find $2 -type f -name *.fd)
  else
    write-out "unzip-bios-installer <bios_file> <destination>" error 1
  fi
}

############################################################
# Recursively finds all files in directory that contain a specified
# text in the first 4 octets in binary form and copies them to a target directory
# Usage: copy-tables <where-to-look> <where-to-copy> <text-to-find>
############################################################
copy-tables()
{
  if [[ $# -ne 3 ]]; then
    write-out "copy-tables <where-to-look> <where-to-copy> <table type>" error 1
  fi

  declare -i ITERATION=1
  IFS=$'\n'
  for i in $(grep -oaPrl $3 $1); do
    xxd -l 4 $i | grep -q $3
    if [[ $? -eq 0 ]]; then
      local EXTENSION=$(basename $i | cut -d '.' -f 2)
      cp $i ${2}/"${3}-${ITERATION}.${EXTENSION}"
      ITERATION+=1
    fi
  done
}

############################################################
# Print help section of the script
############################################################
ept-help()
{
  # Display ept-help
  write-out "Manipulate ACPI tables easily from raw bios files/installers."
  write-out
  write-out "Syntax: ${USAGE}"
  write-out "options:"
  write-out "h     Print this ept-help."
  write-out "v     Verbose output."
  write-out "e     Extract only."
  write-out "V     Print software version and exit."
  write-out "d     Extract bios and decompile tables into ASLs"
  write-out "a     Perform all actions"
  write-out
}

############################################################
# Wrapper function for printing to shell output
# Usage: write-out <text-to-print> <log-level> <exit-option>
############################################################
write-out()
{
  local RESET="\033[0m"
  local COLOR=""

  case $2 in
    warn)
      local COLOR="\033[33m";;
    success)
      local COLOR="\033[32m";;
    error)
      local COLOR="\033[31m";;
  esac

  printf "${COLOR}$1${RESET}\n"

  if [[ $3 -eq 1 ]]; then
    exit 1
  fi
}

############################################################
# Print about section with information
############################################################
print-about()
{
  write-out "${SUBJECT} v${VERSION}"
  write-out "Please submit any issue on https://gitlab.com/RobusTetus/EasyPowerTables/-/issues"
  write-out
  write-out "Developed with ❤️ and a LOT of ☕ by ${AUTHOR}."
}

############################################################
# Function to check if uefiextract is installed
# if not installed, checks environment variable CUSTOM_UEFIEXTRACT
# and copies it to /usr/bin/ directory to be used in place of uefiextract
# alternatively exits the script if uefiextract is not present anywhere
############################################################
check-uefiextract()
{
  if [[ ! $(command -v uefiextract) ]]; then
    if [[ -x $CUSTOM_UEFIEXTRACT ]]; then
      cp $CUSTOM_UEFIEXTRACT /usr/bin/uefiextract
    else
      write-out "UEFIExtract is not present on system! Exiting..." error 1
    fi
  fi
}

############################################################
# Wrapper function for -e option
# Usage: only-extract-tables <path-to-uefi-installer.exe>
############################################################
only-extract-tables()
{
  if [[ $# -ne 1 ]]; then
    write-out "only-extract-tables <bios_executable_installer>" error 1
  fi

  check-uefiextract
  mkdir -p $LOG_DIR
  mkdir -p $EXTRACTED_INSTALLER
  unzip-bios-installer $1 $EXTRACTED_INSTALLER
  mkdir -p $EXTRACTED_BIOS
  disassemble-raw-bios $BIOS_BINARY $EXTRACTED_BIOS
  mkdir -p $RAW_TABLES
  copy-tables $EXTRACTED_BIOS $RAW_TABLES "DSDT"
  copy-tables $EXTRACTED_BIOS $RAW_TABLES "SSDT"
}

############################################################
# Main program that runs everytime script is executed
############################################################

while getopts "hVve:d:a:" option; do
  case $option in
    h) # Display help
      ept-help;
      exit;;
    V) # Display about info
      print-about;
      exit;;
    v) # Verbosity option
      VERBOSE=1;;
    e) # Only extract tables
      only-extract-tables $OPTARG;
      exit;;
    d) # Extract tables and decompile them
      only-extract-tables $OPTARG;
      decompile-tables $RAW_TABLES $DECOMP_TABLES;
      exit;;
    a) # Perform all actions
      only-extract-tables $OPTARG;
      mkdir -p $DECOMP_TABLES;
      decompile-tables $RAW_TABLES $DECOMP_TABLES;
      find-duplicates;
      local DUPLICATE=$?;
      until [[ $DUPLICATE -eq 0 ]]; do delete-text; find-duplicates; DUPLICATE=$?; done;
      exit;;
    \?) # Invalid option
      write-out;
      ept-help;
      exit;;
  esac
done
