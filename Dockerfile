FROM ubuntu:latest
RUN apt update && apt upgrade -y && apt install -y \
acpica-tools \
uefitool-cli \
7zip \
xxd \
findutils \
grep
COPY ept.sh /usr/bin/ept
RUN chmod +x /usr/bin/ept
RUN apt clean all
ENTRYPOINT ["ept"]
